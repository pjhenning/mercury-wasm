#!/bin/bash

MODULES=()
MAIN=""
STANDALONE="n"

while getopts "w:e:m:i:s:" OPTION
do
	case $OPTION in
		w)
			WRAPPER="$OPTARG"
			;;
		e)
			EXPF="$OPTARG"
			;;
		m)
			MAIN="$OPTARG"
			;;
		i)
			MODULES+=("$OPTARG")
			;;
		s)
			STANDALONE="$OPTARG"
			;;
	esac
done

if [ "$MAIN" = "" ]; then
	echo "Main file is missing; specify with \`-m FILENAME\`"
	exit
fi

docker run -t --rm \
	-v $(pwd):/mmcw \
	--entrypoint "/bin/bash" \
	pjhenning/mercury-wasm:0.2 \
	/handlecomp.sh $EXPF $WRAPPER $MAIN $STANDALONE "${MODULES[@]}"
