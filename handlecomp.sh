#!/bin/bash

CLARGS=("$@")

EXPF=${CLARGS[0]}
unset CLARGS[0]
WRAPPER=${CLARGS[1]}
WRAPPER="${WRAPPER%.c}"
unset CLARGS[1]
MFILE=${CLARGS[2]}
MFILE_BASE=${MFILE%".m"}
MFILE_PATH=/mmcw/${MFILE_BASE}
unset CLARGS[2]
STANDALONE=${CLARGS[3]}
unset CLARGS[3]
TMP_PATH=/mwtmp
INIT_BASE=${MFILE_BASE}_init

mkdir $TMP_PATH

cp ${MFILE_PATH}.m ${TMP_PATH}/${MFILE_BASE}.m
cp /mmcw/${WRAPPER}.c /${TMP_PATH}/${WRAPPER}.c

for i in "${CLARGS[@]}"
do
	cp /mmcw/${i} ${TMP_PATH}/${i}
done

cd $TMP_PATH

mmc \
	--infer-all \
	--make-short-int \
	"${CLARGS[@]}"
[ $? -eq 0 ]  || exit 1
mmc \
	--infer-all \
	--make-priv-int \
	"${CLARGS[@]}"
[ $? -eq 0 ]  || exit 1
mmc \
	--infer-all \
	--make-int \
	"${CLARGS[@]}"
[ $? -eq 0 ]  || exit 1

for i in "${CLARGS[@]}"
do
	mmc \
		--no-main \
		--infer-all \
		--high-level-code \
		--gc boehm \
		--tags low \
		--num-tag-bits 2 \
		--compile-to-c \
		${i}
	[ $? -eq 0 ]  || exit 1
done

MODULE_BASES=()
MODULE_CS=()
MODULE_OBJS=()

for i in "${CLARGS[@]}"
do
	NEXT_BASE=${i%".m"}
	NEXT_C=${NEXT_BASE}.c
	NEXT_OBJ=${NEXT_BASE}.o
	MODULE_BASES+=("$NEXT_BASE")
  MODULE_CS+=("$NEXT_C")
	MODULE_OBJS+=("$NEXT_OBJ")
done

mmc \
	--no-main \
	--search-library-files-directory . \
	--infer-all \
	--high-level-code \
	--gc boehm \
	--tags low \
	--num-tag-bits 2 \
	--compile-to-c \
	${MFILE_BASE}.m
[ $? -eq 0 ]  || exit 1

c2init --no-main --high-level-code --gc boehm --mercury-stdlib-dir /lib/mercury ${MFILE_BASE}.c "${MODULE_CS[@]}" > ${INIT_BASE}.c
[ $? -eq 0 ]  || exit 1

#EMCC_DEBUG=1 \
emcc -g \
	-s ALLOW_MEMORY_GROWTH=1 \
	-s STACK_OVERFLOW_CHECK=2 \
	-I/mercury-srcdist-20.01.1/library/  \
	-I/mercury-srcdist-20.01.1/runtime/ \
	-I/mercury-srcdist-20.01.1/boehm_gc/ \
	-I/mercury-srcdist-20.01.1/boehm_gc/include/ \
	-fomit-frame-pointer \
	-fno-strict-aliasing \
	-DMR_HIGHLEVEL_CODE \
	-DMR_CONSERVATIVE_GC \
	-DMR_BOEHM_GC \
	-DMR_TAGBITS=2 \
	${INIT_BASE}.c -o ${INIT_BASE}.o
[ $? -eq 0 ]  || exit 1

for i in "${MODULE_BASES[@]}"
do
	#EMCC_DEBUG=1 \
	emcc -g \
		-s ALLOW_MEMORY_GROWTH=1 \
		-s STACK_OVERFLOW_CHECK=2 \
		-I/mercury-srcdist-20.01.1/library/ \
		-I/mercury-srcdist-20.01.1/runtime/ \
		-I/mercury-srcdist-20.01.1/boehm_gc/ \
		-I/mercury-srcdist-20.01.1/boehm_gc/include/ \
		-fomit-frame-pointer \
		-fno-strict-aliasing \
		-DMR_HIGHLEVEL_CODE \
		-DMR_CONSERVATIVE_GC \
		-DMR_BOEHM_GC \
		-DMR_TAGBITS=2 \
		${i}.c -o ${i}.o
	[ $? -eq 0 ]  || exit 1
done

#EMCC_DEBUG=1 \
emcc -g \
	-s ALLOW_MEMORY_GROWTH=1 \
	-s STACK_OVERFLOW_CHECK=2 \
	-I/mercury-srcdist-20.01.1/library/ \
	-I/mercury-srcdist-20.01.1/runtime/ \
	-I/mercury-srcdist-20.01.1/boehm_gc/ \
	-I/mercury-srcdist-20.01.1/boehm_gc/include/ \
	-fomit-frame-pointer \
	-fno-strict-aliasing \
	-DMR_HIGHLEVEL_CODE \
	-DMR_CONSERVATIVE_GC \
	-DMR_BOEHM_GC \
	-DMR_TAGBITS=2 \
	${MFILE_BASE}.c -o ${MFILE_BASE}.o
[ $? -eq 0 ]  || exit 1

#EMCC_DEBUG=1 \
emcc -g \
	-s ALLOW_MEMORY_GROWTH=1 \
	-s STACK_OVERFLOW_CHECK=2 \
	-I/mercury-srcdist-20.01.1/library/ \
	-I/mercury-srcdist-20.01.1/runtime/ \
	-I/mercury-srcdist-20.01.1/boehm_gc/ \
	-I/mercury-srcdist-20.01.1/boehm_gc/include/ \
	-fomit-frame-pointer \
	-fno-strict-aliasing \
	-DMR_HIGHLEVEL_CODE \
	-DMR_CONSERVATIVE_GC \
	-DMR_BOEHM_GC \
	-DMR_TAGBITS=2 \
	${WRAPPER}.c -o ${WRAPPER}.o
[ $? -eq 0 ]  || exit 1

if [ "$STANDALONE" = "n" ]; then
	#EMCC_DEBUG=1 \
	emcc -g4 \
		-s ERROR_ON_UNDEFINED_SYMBOLS=0 \
		-s ALLOW_MEMORY_GROWTH=1 \
		-s STACK_OVERFLOW_CHECK=2 \
		-s EXIT_RUNTIME=1 \
		-s MODULARIZE=1 \
		-s 'EXPORT_NAME="PAT"' \
		-s "EXPORTED_FUNCTIONS=['_${EXPF}']" \
		-s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall", "cwrap"]' \
		--source-map-base "http://localhost:8080/" \
		-o /mmcw/${MFILE_BASE}.js \
		${WRAPPER}.o \
		${INIT_BASE}.o \
		${MFILE_BASE}.o \
		"${MODULE_OBJS[@]}" \
		/mercury-srcdist-20.01.1/library/libmer_std.a \
		/mercury-srcdist-20.01.1/runtime/libmer_rt.a \
		/mercury-srcdist-20.01.1/boehm_gc/libgc.a
	[ $? -eq 0 ]  || exit 1
else
	#EMCC_DEBUG=1 \
	emcc -g \
		-s ERROR_ON_UNDEFINED_SYMBOLS=0 \
		-o /mmcw/${MFILE_BASE}.wasm \
		${WRAPPER}.o \
		${INIT_BASE}.o \
		${MFILE_BASE}.o \
		"${MODULE_OBJS[@]}" \
		/mercury-srcdist-20.01.1/library/libmer_std.a \
		/mercury-srcdist-20.01.1/runtime/libmer_rt.a \
		/mercury-srcdist-20.01.1/boehm_gc/libgc.a
	[ $? -eq 0 ]  || exit 1
fi

sed -i 's/\.\.\///g' /mmcw/${MFILE_BASE}.wasm.map
#sed -i 's/pat_w\.m/pat_w\.mh/g' /mmcw/${MFILE_BASE}.wasm.map

cp ${MFILE_BASE}.c /mmcw/${MFILE_BASE}.c
cp ${INIT_BASE}.c /mmcw/${INIT_BASE}.c
for i in "${MODULE_BASES[@]}"
do
	cp ${i}.c /mmcw/${i}.c
done
