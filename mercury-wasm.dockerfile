ARG MERCURY_DEV_PREFIX=/usr/local/mercury

FROM debian:10

SHELL ["/bin/bash", "-c"]

ENV PATH ${MERCURY_DEV_PREFIX}/bin:$PATH

RUN apt update && \
	apt install -y apt-utils pkg-config && \
	apt install -y \
		build-essential \
		clang \
		curl \
		flex \
		bison \ 
		git \
		python3 \
		python3-distutils \
		cmake \
		default-jre \
		autoconf \
		libtool && \
	curl -o mercury.tar.gz http://dl.mercurylang.org/release/mercury-srcdist-20.01.1.tar.gz

# Bootstrap minimal install
RUN	tar -zxf mercury.tar.gz && \
	cd ./mercury-srcdist-20.01.1 && \
	./configure \
		--enable-minimal-install \
		--prefix=$MERCURY_DEV_PREFIX && \
	CORES=$(nproc --all) && \
	make PARALLEL=-j$CORES && \
	make install PARALLEL=-j$CORES && \
	cd .. && \
	rm -rf mercury-srcdist-20.01.1

# Build full native install
RUN tar -zxf mercury.tar.gz && \
	cd ./mercury-srcdist-20.01.1 && \
	./configure \
		--disable-dynamic-link \
		--with-cc=clang \
		--enable-libgrades=hlc.gc \
		--with-default-grade=hlc.gc \
		--disable-dynamic-link \
		--prefix=$MERCURY_DEV_PREFIX && \
	CORES=$(nproc --all) && \
	make PARALLEL=-j$CORES && \
	make install PARALLEL=-j$CORES

RUN cp ./mercury-srcdist-20.01.1/scripts/mgnuc ./mgnuc_native && \
	cp -r ./mercury-srcdist-20.01.1/util ./native_util && \
	rm -rf mercury-srcdist-20.01.1

# Setup emscripten
RUN git clone https://github.com/emscripten-core/emsdk.git && \
	cd emsdk && git pull && \
	./emsdk install sdk-upstream-master-64bit && \
	./emsdk activate sdk-upstream-master-64bit
	
RUN cd emsdk/emscripten/master && \
	git remote add stfork https://github.com/pjhenning/emscripten.git && \
	git fetch stfork && \
	git checkout -b newmaster --track stfork/conditional-stubs-stable
		
ENV PATH="/emsdk:/emsdk/node/12.9.1_64bit/bin:/emsdk/emscripten/master:${PATH}"

# Build GC lib for WASM
RUN tar -zxf mercury.tar.gz && \
	cd ./mercury-srcdist-20.01.1/boehm_gc && \
	./autogen.sh && \
	emconfigure ./configure \
		--disable-threads \
		--disable-sigrt-signals \
		--disable-parallel-mark && \
	sed -i \
		's/ABORT_ARG1("GC_is_visible test failed", ": %p not GC-visible", p)/printf("GC_is_visible test failed: %p\\n", p)/' \
		./ptr_chck.c && \
	EMCC_DEBUG=1 emmake make && \
	cp ./.libs/libgc.a ./libgc.a && \
	cp ./.libs/libgc.so ./libgc.so

COPY ./configure.ac /mercury-srcdist-20.01.1/configure.ac

COPY ./Mmakefile.root /mercury-srcdist-20.01.1/Mmakefile

# Build Mercury libs for WASM
RUN cd ./mercury-srcdist-20.01.1 && \
	autoconf --force && \
	sed -i \
		's/SIG_STUBS_SIGACTION = 0/SIG_STUBS_SIGACTION = 1/' \
		/emsdk/emscripten/master/src/settings.js && \
	sed -i \
		's/SIG_STUBS_RAISE = 0/SIG_STUBS_RAISE = 1/' \
		/emsdk/emscripten/master/src/settings.js && \
	emconfigure ./configure \
		--enable-libgrades=hlc.gc  \
		--with-default-grade=hlc.gc \
		--disable-dynamic-link

COPY ./INT64.Diff /mercury-srcdist-20.01.1/INT64.Diff

RUN rm -rf ./mercury-srcdist-20.01.1/util && \
	cp -r ./native_util ./mercury-srcdist-20.01.1/util && \
	chmod +x /mercury-srcdist-20.01.1/util/mkinit && \
	cd ./mercury-srcdist-20.01.1 && \
	patch -p1 < ./INT64.Diff && \
	EMCC_DEBUG=1 emmake make

COPY ./handlecomp.sh /handlecomp.sh

RUN chmod +x handlecomp.sh

